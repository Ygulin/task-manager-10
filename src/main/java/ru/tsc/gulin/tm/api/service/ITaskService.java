package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    void remove(Task task);

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    void clear();

}
