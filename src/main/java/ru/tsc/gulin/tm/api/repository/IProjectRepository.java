package ru.tsc.gulin.tm.api.repository;

import ru.tsc.gulin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void remove(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    void clear();

}
